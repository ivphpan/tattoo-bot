<?php


namespace App\Components;


class Text
{
    public static function make($text): string
    {
        $text = preg_replace('#( {2,})#', '', $text);
        $text = preg_replace('#^([^a-zа-я\s\<]+)([a-zа-я\.\n\s0-9]+)#iu', '$2', $text);
        $text = trim($text);
        return $text;
    }

    public static function ucfirst($string): string
    {
        return mb_convert_case($string, MB_CASE_TITLE, 'UTF-8');
    }
}