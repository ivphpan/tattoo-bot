<?php


namespace App\Components;


use App\Models\TelegramUser;
use App\Models\TelegramUserData;

class UserData
{
    private TelegramUser $user;
    private array $list = [];

    public function __construct(TelegramUser $user)
    {
        $this->user = $user;
    }

    public function set($key, $value): bool|TelegramUserData
    {
        return TelegramUserData::updateOrCreate(
            [
                'telegram_user_id' => $this->user->id,
                'key'=>$key,
            ],
            [
                'telegram_user_id' => $this->user->id,
                'key' => $key,
                'value' => $value
            ]
        );
    }

    public function get($key): string|null
    {
        if (empty($this->list)) {
            $this->collected();
        }

        return array_key_exists($key, $this->list) ? $this->list[$key] : null;
    }

    private function collected(): void
    {
        $items = $this->user->data()->get();
        foreach ($items as $item) {
            $this->list[$item->key] = $item->value;
        }
    }

    public function destroy(): void
    {
        $this->user->data()->delete();
    }
}