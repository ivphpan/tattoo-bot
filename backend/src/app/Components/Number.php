<?php


namespace App\Components;


class Number
{
    public static function make($string)
    {
        if (is_numeric($string)) {
            return $string;
        }

        $number = $string;
        $numberPrefix = substr($string, 0, 1)=='-' ? '-' : '';
        $numberPostfix = '';

        $number = str_replace(['б', 'ю', ','], '.', $number);
        $number = preg_replace('#[^\d\.]+#u', '', $number);

        if (empty($number)) {
            return 0;
        }

        if (preg_match('#(.*)(\.\d+)$#u', $number, $matches)) {
            $number = preg_replace('#\.#u', '', $matches[1]);
            $endNumberOfPrefix = $matches[2];
            $endNumber = substr($endNumberOfPrefix, 1);
            $numberPostfix = $endNumber>0 ? $endNumberOfPrefix : $endNumber;
        }

        return $numberPrefix . $number . $numberPostfix;
    }

    public static function format($number)
    {
        $number = number_format($number, '2', '.', ' ');
        $number = preg_replace('#\.0{1,}$#u', '', $number);
        return $number;
    }
}