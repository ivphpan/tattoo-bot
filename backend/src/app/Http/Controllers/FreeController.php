<?php

namespace App\Http\Controllers;

use App\Telegram\Api;
use Illuminate\Http\Request;

class FreeController extends Controller
{
    public function index()
    {

        $smileList = ['😐', '😕', '😳', '😒', '😞', '😔', '😟', '☹', '️😖', '😫', '😭', '😑', '😐', '🤔', '😵'];
        dump($smileList[mt_rand(0, sizeof($smileList)-1)]);

        $isRead = 1 << 0;
        $isCreate = 1 << 1;
        $isUpdate = 1 << 2;
        $isDelete = 1 << 3;
        $isAll = $isRead | $isDelete | $isCreate | $isUpdate;

        dump([
            'isRead' => sprintf('%1$04d = %1$04b', $isRead),
            'isCreate' => sprintf('%1$04d = %1$04b', $isCreate),
            'isUpdate' => sprintf('%1$04d = %1$04b', $isUpdate),
            'isDelete' => sprintf('%1$04d = %1$04b', $isDelete),
            'isAll' => sprintf('%1$04d = %1$04b', $isAll),
        ]);

        $password = 'ivp123';
        $key = '=)^*';
        $passwordChars = str_split($password);
        $keyChars = str_split($key);
        $keyCharsSize = sizeof($keyChars);
        $encodeString = '';
        foreach ($passwordChars as $i => $char) {
            $charCode = ord($char);
            $keyCode = ord($keyChars[$i % $keyCharsSize]);
            $encodeString .= $charCode ^ $keyCode;
        }

        dump([
            'password' => $password,
            'passwordChars' => $passwordChars,
            'key' => $key,
            'keyChars' => $keyChars,
            'keyCharsSize' => $keyCharsSize,
            'encodeString' => $encodeString,
        ]);

        $countBusyDay = 25;
        $startBusyDay = 15;
        $busyDayBinaryMask = 0;
        $busyDayList = [];
        for($i=$startBusyDay;$i<=$countBusyDay; $i++){
            $binaryDay = 1 << $i-1;
            $busyDayBinaryMask |= $binaryDay;
            $busyDayList[$i] = sprintf('%02d', $i);
        }

        $countDayOfMonth = 31;
        $dayMonthList = [];
        $dayMonthMask = 0;
        for ($i = 1; $i < $countDayOfMonth; $i++) {
            $dayBinary = 1 << $i-1;
            $dayMonthList[$i] = [
                'day' => $i,
                'binary' => sprintf('%08b', $dayBinary),
                'isBusy'=> ($dayBinary&$busyDayBinaryMask) > 0,
            ];
            $dayMonthMask |= $dayBinary;
        }

        dump([
            'dayMonthMask'=> sprintf('%032b', $dayMonthMask),
            'busyDayList'=> implode('|', $busyDayList),
            'busyDayBinaryMask'=>sprintf('%1$04d = %1$031b', $busyDayBinaryMask),
            'dayMonthList'=> $dayMonthList,
        ]);
    }
}
