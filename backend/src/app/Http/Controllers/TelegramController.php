<?php


namespace App\Http\Controllers;


use App\Telegram\Application;

class TelegramController extends Controller
{
    public function handleWebhook()
    {
        (new Application())->webhookHandle(file_get_contents('php://input'));
    }

    public function server($method) {}
}