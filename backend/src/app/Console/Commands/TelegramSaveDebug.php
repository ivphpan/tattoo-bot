<?php

namespace App\Console\Commands;

use App\Telegram\Application;
use Illuminate\Console\Command;

class TelegramSaveDebug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:save-debug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Сохраняет обновления в формате JSON для последующего дебага';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new Application())->saveDebug();
    }
}
