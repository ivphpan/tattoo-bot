<?php

namespace App\Console\Commands;

use App\Telegram\Application;
use Illuminate\Console\Command;

class TelegramDebug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:debug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает обработку сообщений из полученного дампа';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new Application())->debug();
    }
}
