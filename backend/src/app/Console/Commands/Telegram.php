<?php

namespace App\Console\Commands;

use App\Telegram\Application;
use Illuminate\Console\Command;

class Telegram extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:loop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Запускает сервер по получению и обработке сообщений от телеграм-бота';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        (new Application())->loop();
    }
}
