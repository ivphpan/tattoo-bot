<?php


namespace App\Telegram;


use App\Telegram\Controllers\ControllerAbstract;
use App\Telegram\Handlers\HandlerCollection;
use App\Telegram\Handlers\StartHandler;
use App\Telegram\Updates\CallbackQueryUpdate;
use App\Telegram\Updates\MessageUpdate;
use App\Telegram\Updates\UpdateAbstract;
use App\Telegram\Updates\UpdateFactory;
use Illuminate\Support\Facades\Storage;

class Application
{
    private Api $api;
    const TIME_UPDATE = 1000;//ms

    public function __construct()
    {
        $this->api = new Api(
            config('telegram.token')
        );
    }

    public function loop(): void
    {
        $this->getUpdates(function (UpdateAbstract $update) {
            $this->work($update);
        });
    }

    public function saveDebug()
    {
        $this->api
            ->setTimeout(5)
            ->setMethod('getUpdates');
        $response = $this->api->call();
        if ($response->isOk()) {
            Storage::put('telegram_dump/'.time() . '.json', json_encode($response->getResult(), JSON_UNESCAPED_UNICODE |
                JSON_PRETTY_PRINT));
        }
    }

    public function debug()
    {
        $filename = '1623827209.json';
        $content = Storage::get('telegram_dump/'.$filename);
        $updates = json_decode($content);
        foreach ($updates as $update) {
            $updateObject = UpdateFactory::make($update);
            if ($updateObject->getType()) {
                $this->work($updateObject);
            }
        }
    }

    private function getUpdates($callback)
    {
        $offset = 0;
        $this->api
            ->setTimeout(5)
            ->setMethod('getUpdates');

        while (true) {
            $response = $this->api
                ->setParams([
                    'offset' => $offset,
                ])
                ->call();
            if (!empty($response->isOk())) {
                foreach ($response->getResult() as $update) {
                    $updateObject = UpdateFactory::make($update);
                    $offset = $updateObject->updateId() + 1;

                    if ($updateObject->getType()) {
                        $callback($updateObject);
                    }
                }
            }

            usleep(self::TIME_UPDATE * 1000);
        }
    }

    public function setWebhook()
    {
        $response = (new Api())
            ->setMethod('setWebhook')
            ->setParams([
                'url' => env('TELEGRAM_WEBHOOK_URL'),
                'allowed_updates' => [
                    'message',
                    'callback_query',
                ],
            ])
            ->setPostParams([
                'certificate' => '@'.env('TELEGRAM_SSL_PEM'),
            ])
            ->call();
        dump('Telegram:setWebhook', $response);
    }

    public function deleteWebhook()
    {
        $response = (new Api())
            ->setMethod('deleteWebhook')
            ->call();
        dump('Telegram:deleteWebhook', $response);
    }

    public function webhookHandle($input)
    {
        $input = json_decode($input);
        $updateObject = UpdateFactory::make($input);
        if ($updateObject->getType()) {
            $this->work($updateObject);
        }
    }

    private function work(UpdateAbstract $update): bool
    {
        $controllerName = $update->getUser()->controller;
        $actionName = $update->getUser()->action;
        $messageHandlerCollection = new HandlerCollection(MessageUpdate::TYPE);
        $callbackHandlerCollection = new HandlerCollection(CallbackQueryUpdate::TYPE);

        $messageHandlerCollection->addClass('#/start#', new StartHandler());

        if (!class_exists($controllerName)) {
            $messageHandlerCollection->call($update);
            return false;
        }

        /** @var  $controller ControllerAbstract */
        $controller = new $controllerName($messageHandlerCollection, $callbackHandlerCollection);
        $controller->beforeAction();
        if ($actionName && method_exists($controller, $actionName)) {
            $controller->$actionName();
        } else {
            dump('Не найден экшен '.$actionName.' в контроллере '.$controllerName);
        }

        if ($messageHandlerCollection->call($update)) {
            return true;
        } elseif ($callbackHandlerCollection->call($update)) {
            return true;
        }

        $update->answerWtf();
        return false;
    }
}
