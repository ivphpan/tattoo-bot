<?php


namespace App\Telegram\Keyboards;


abstract class KeyboardAbstract
{
    protected array $buttons = [];
    protected int $line = 0;

    public function newLine():self
    {
        $this->buttons[++$this->line] = [];
        return $this;
    }

    abstract public function generate(): string;
}