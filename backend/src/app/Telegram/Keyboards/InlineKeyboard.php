<?php


namespace App\Telegram\Keyboards;


class InlineKeyboard extends KeyboardAbstract
{
    public static function make(): static
    {
        return new static();
    }

    public function button($text, $data = '', $url = ''):self
    {
        $this->buttons[$this->line][] = [
            'text' => $text,
            'url' => $url,
            'callback_data' => $data,
        ];

        return $this;
    }

    public function generate(): string
    {
        return json_encode([
            'inline_keyboard' => $this->buttons,
        ]);
    }
}