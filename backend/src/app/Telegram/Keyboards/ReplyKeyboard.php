<?php


namespace App\Telegram\Keyboards;


class ReplyKeyboard extends KeyboardAbstract
{
    public function __construct(
        private bool $resizeKeyboard = true,
        private bool $oneTimeKeyboard = false
    ) {
    }

    public static function make($resizeKeyboard = true, $oneTimeKeyboard = false): static
    {
        return new static($resizeKeyboard, $oneTimeKeyboard);
    }

    public function button($text, $requestContact = false, $requestLocation = false):self
    {
        $this->buttons[$this->line][] = [
            'text' => $text,
            'request_contact' => $requestContact,
            'request_location' => $requestLocation
        ];

        return $this;
    }

    public function generate(): string
    {
        return json_encode([
            'keyboard' => $this->buttons,
            'resize_keyboard' => $this->resizeKeyboard,
            'one_time_keyboard' => $this->oneTimeKeyboard
        ]);
    }
}