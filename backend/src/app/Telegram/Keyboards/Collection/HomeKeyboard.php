<?php


namespace App\Telegram\Keyboards\Collection;


use App\Components\Number;
use App\Models\TelegramUser;
use App\Telegram\Keyboards\KeyboardAbstract;
use App\Telegram\Keyboards\ReplyKeyboard;

class HomeKeyboard extends KeyboardAbstract
{
    public function __construct(
        private TelegramUser $user
    ) {
    }

    public function generate(): string
    {
        return ReplyKeyboard::make()
            ->button('💳 Ваш баланс: '. Number::format($this->user->balance) .' '. $this->user->currency)
            ->newLine()
            ->button('➖ Расход')
            ->button('📊 Отчёт')
            ->button('➕ Доход')
            ->newLine()
            ->button('⚙️ Настройки')
            ->newLine()
            ->button('💸 Поддержать автора')
            ->generate();
    }
}