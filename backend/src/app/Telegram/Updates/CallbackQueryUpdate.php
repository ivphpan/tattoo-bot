<?php


namespace App\Telegram\Updates;


use App\Telegram\Api;
use stdClass;

class CallbackQueryUpdate extends UpdateAbstract
{
    const TYPE = 'callback_query';

    public function __construct($update)
    {
        parent::__construct($update);
        $this->answerCallBackQuery($this->getCallbackQueryId());
    }

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getText(): string
    {
        return $this->update->callback_query->data;
    }

    public function getMessageId(): int
    {
        return $this->update->callback_query->message->message_id;
    }

    public function getFrom(): stdClass|null
    {
        return $this->update->callback_query->from;
    }

    private function answerCallBackQuery($id):bool
    {
        $response = (new Api())
            ->setMethod('answerCallbackQuery')
            ->setParams([
                'callback_query_id'=>$id,
            ])
            ->call();

        return property_exists($response, 'ok');
    }

    private function getCallbackQueryId():int
    {
        return $this->update->callback_query->id;
    }
}