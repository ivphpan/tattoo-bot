<?php


namespace App\Telegram\Updates;


use App\Models\TelegramUser;
use stdClass;

class NullUpdate extends UpdateAbstract
{
    public function getType(): string
    {
        return '';
    }

    public function getUser(): TelegramUser|null
    {
        return null;
    }

    public function getText(): string
    {
        return '';
    }

    public function getMessageId(): int
    {
        return 0;
    }

    public function getFrom(): stdClass|null
    {
        return null;
    }
}