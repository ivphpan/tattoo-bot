<?php


namespace App\Telegram\Updates;


class UpdateFactory
{
    public static function make($update): UpdateAbstract
    {
        if (property_exists($update, 'message') && property_exists($update->message, 'text')) {
            return new MessageUpdate($update);
        } elseif (property_exists($update, 'callback_query')) {
            return new CallbackQueryUpdate($update);
        } else {
            return new NullUpdate($update);
        }
    }
}
