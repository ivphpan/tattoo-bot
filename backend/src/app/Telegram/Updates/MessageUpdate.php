<?php


namespace App\Telegram\Updates;


use stdClass;

class MessageUpdate extends UpdateAbstract
{
    const TYPE = 'message';

    public function getType(): string
    {
        return self::TYPE;
    }

    public function getText():string
    {
        return $this->update->message->text;
    }

    public function getMessageId(): int
    {
        return $this->update->message->message_id;
    }

    public function getFrom(): stdClass
    {
        return $this->update->message->from;
    }
}