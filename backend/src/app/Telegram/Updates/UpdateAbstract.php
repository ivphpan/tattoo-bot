<?php


namespace App\Telegram\Updates;


use App\Models\TelegramUser;
use App\Telegram\Api;
use App\Components\UserData;
use App\Telegram\Components\Response;
use App\Telegram\Controllers\RegisterController;
use \stdClass;
use App\Components\Text;

abstract class UpdateAbstract
{
    public const MODE_HTML = 'HTML';
    public const MODE_MARKDOWN = 'MarkdownV2';

    protected stdClass $update;
    protected TelegramUser|null $user = null;
    protected UserData|null $userData = null;

    public function __construct($update)
    {
        $this->update = $update;
    }

    public function updateId(): int
    {
        return $this->update->update_id;
    }

    public function answer(
        $text,
        $keyboard = null,
        $parseMode = '',
        $disableWebPagePreview = false,
        $disable_notification = false
    ): Response {

        if ($keyboard) {
            $keyboard = $keyboard->generate();
        }

        $text = Text::make($text);

        $this->sendTypingAction();

        return (new Api())
            ->setMethod('sendMessage')
            ->setParams([
                'chat_id' => $this->getUser()->id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => $parseMode,
                'disable_web_page_preview' => $disableWebPagePreview,
                'disable_notification' => $disable_notification,
            ])
            ->call();
    }

    public function answerWtf()
    {
        $smileList = ['😐', '😕', '😳', '😒', '😞', '😔', '😟', '☹', '️😖', '😫', '😭', '😑', '😐', '🤔', '😵'];
        $text = $smileList[mt_rand(0, sizeof($smileList) - 1)];

        $this->sendTypingAction();

        return (new Api())
            ->setMethod('sendMessage')
            ->setParams([
                'chat_id' => $this->getUser()->id,
                'text' => $text,
            ])
            ->call();
    }

    protected function sendTypingAction()
    {
        if (mt_rand(1, 2) == 2) {
            (new Api())
                ->setMethod('sendChatAction')
                ->setParams([
                    'chat_id' => $this->getUser()->id,
                    'action' => 'typing',
                ])
                ->call();
        }
    }

    public function editMessage(
        $text,
        $keyboard = null,
        $parseMode = '',
        $disableWebPagePreview = false,
        $messageId = 0
    ): Response {

        if ($keyboard) {
            $keyboard = $keyboard->generate();
        }

        $text = Text::make($text);

        $params = [
            'chat_id' => $this->getUser()->id,
            'message_id' => $messageId ? $messageId : $this->getMessageId(),
            'text' => $text,
            'reply_markup' => $keyboard,
            'parse_mode' => $parseMode,
            'disable_web_page_preview' => $disableWebPagePreview,
        ];

        return (new Api())
            ->setMethod('editMessageText')
            ->setParams($params)
            ->call();
    }

    public function editPreviousMessage(
        $text,
        $keyboard = null,
        $parseMode = '',
        $disableWebPagePreview = false
    ): Response {
        return $this->editMessage($text, $keyboard, $parseMode, $disableWebPagePreview, $this->getPreviousMessageId());
    }

    public function delete($messageId = 0): bool
    {
        $response = (new Api())
            ->setMethod('deleteMessage')
            ->setParams([
                'chat_id' => $this->getUser()->id,
                'message_id' => $messageId ? $messageId : $this->getMessageId(),
            ])
            ->call();

        return $response->isOk();
    }

    public function previousDelete()
    {
        return $this->delete($this->getPreviousMessageId());
    }

    public function getUser(): TelegramUser|null
    {
        if ($this->user !== null) {
            return $this->user;
        }

        $from = $this->getFrom();
        $this->user = TelegramUser::whereId($from->id)->first();
        $username = property_exists($from, 'username') ? $from->username : '';
        if (!$this->user) {
            $firstname = property_exists($from, 'first_name') ? $from->first_name : '';
            $lastname = property_exists($from, 'last_name') ? $from->last_name : '';

            $this->user = TelegramUser::create([
                'id' => $this->getFrom()->id,
                'username' => $username,
                'first_name' => $firstname,
                'last_name' => $lastname,
            ]);
        }

        return $this->user;
    }

    public function getUserData(): UserData
    {
        if (!$this->userData) {
            $this->userData = new UserData($this->getUser());
        }

        return $this->userData;
    }

    public function setPreviousMessageId($messageId): void
    {
        $this->getUserData()->set('previousMessageId', $messageId);
    }

    public function getPreviousMessageId(): int|null
    {
        return $this->getUserData()->get('previousMessageId');
    }

    abstract public function getFrom(): stdClass|null;

    abstract public function getText(): string;

    abstract public function getType(): string;

    abstract public function getMessageId(): int;
}
