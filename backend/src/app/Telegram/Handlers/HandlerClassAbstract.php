<?php


namespace App\Telegram\Handlers;


use App\Telegram\Updates\UpdateAbstract;

abstract class HandlerClassAbstract
{
    abstract public function call(UpdateAbstract $update):bool;
}