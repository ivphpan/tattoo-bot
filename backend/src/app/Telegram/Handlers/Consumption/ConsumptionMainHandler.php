<?php


namespace App\Telegram\Handlers\Consumption;


use App\Models\TelegramUserConsumption;
use App\Repository\TelegramUserConsumptionRepository;
use App\Telegram\Controllers\ConsumptionController;
use App\Telegram\Handlers\HandlerClassAbstract;
use App\Telegram\Keyboards\ReplyKeyboard;
use App\Telegram\Updates\UpdateAbstract;
use Carbon\Carbon;

class ConsumptionMainHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->getUserData()->destroy();
        $consumptionRepository = new TelegramUserConsumptionRepository();
        $startDate = Carbon::now()->subMonth();
        $endDate = Carbon::now()->addDay();

        $consumptionItems = $consumptionRepository->getLastConsumptionByParams(
            $update->getUser()->id,
            $startDate,
            $endDate,
            6
        );

        if ($consumptionItems->isNotEmpty()) {
            $keyboard = ReplyKeyboard::make();

            foreach ($consumptionItems as $i => $consumptionItem) {
                /** @var $consumptionItem TelegramUserConsumption */
                $keyboard->button(
                    text: $consumptionItem->consumption->name
                );

                if (($i + 1) % 2 == 0) {
                    $keyboard->newLine();
                }
            }

            $keyboard
                ->newLine()
                ->button('🔎 Воспользоваться поиском')
                ->newLine()
                ->button('➕ Добавить статью расходов')
                ->newLine()
                ->button('🔄 Отмена');

            $update->answer(
                text: 'Выберите статью расходов',
                keyboard: $keyboard
            );

            $update->getUser()->update([
                'controller' => ConsumptionController::class,
                'action' => 'select',
            ]);
        } else {
            $update->answer(
                text: 'Введите текст для поиска статьи расходов',
                keyboard: ReplyKeyboard::make()
                ->button('➕ Добавить статью расходов')
                ->newLine()
                ->button('🔄 Отмена')
            );

            $update->getUser()->update([
                'controller' => ConsumptionController::class,
                'action' => 'search',
            ]);
        }

        return true;
    }
}