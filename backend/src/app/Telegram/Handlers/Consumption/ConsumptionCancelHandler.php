<?php


namespace App\Telegram\Handlers\Consumption;


use App\Telegram\Controllers\MainController;
use App\Telegram\Handlers\HandlerClassAbstract;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Updates\UpdateAbstract;

class ConsumptionCancelHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update):bool
    {
        $update->getUserData()->destroy();

        $update->answer('Вы отменили операцию с расходами.', new HomeKeyboard($update->getUser()));

        $update->getUser()->update([
            'controller' => MainController::class,
            'action' => 'index',
        ]);

        return true;
    }
}