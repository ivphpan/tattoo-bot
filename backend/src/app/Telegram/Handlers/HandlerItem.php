<?php


namespace App\Telegram\Handlers;


use App\Telegram\Updates\UpdateAbstract;
use \Closure;

class HandlerItem
{
    public function __construct(
        private $condition,
        private Closure|null $callback = null,
        private HandlerClassAbstract|null $class = null,
    ) {
    }

    public function check($text): bool
    {
        return preg_match($this->condition, $text);
    }

    public function call(UpdateAbstract $update): bool
    {
        $result = false;

        if (!empty($this->class)) {
            $class = new $this->class();

            if ($class->call($update)) {
                $result = true;
            }
        } else {
            $callback = $this->callback;
            if ($callback($update)) {
                $result = true;
            }
        }

        return $result;
    }
}