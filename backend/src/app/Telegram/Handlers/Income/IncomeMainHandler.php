<?php


namespace App\Telegram\Handlers\Income;


use App\Telegram\Controllers\IncomeController;
use App\Telegram\Handlers\HandlerClassAbstract;
use App\Telegram\Keyboards\InlineKeyboard;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;

class IncomeMainHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->getUserData()->destroy();

        $update->answer(
            text: "Введите сумму дохода",
            keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
            parseMode: MessageUpdate::MODE_HTML
        );

        $update->getUser()->update([
            'controller' => IncomeController::class,
            'action' => 'sum',
        ]);

        return true;
    }
}