<?php


namespace App\Telegram\Handlers;


use App\Telegram\Controllers\SettingController;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;

class SettingsHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->answer(
            text: "Выберите вариант настройки",
            keyboard: ReplyKeyboard::make()
            ->button('💵 Настройка валюты', )
            ->newLine()
            ->button('🔄 Отмена', ),
            parseMode: MessageUpdate::MODE_HTML
        );

        $update->getUser()->update([
            'controller'=>SettingController::class,
            'action'=>'index',
        ]);

        return true;
    }
}