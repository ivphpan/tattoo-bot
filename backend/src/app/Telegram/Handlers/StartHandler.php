<?php


namespace App\Telegram\Handlers;

use App\Components\Number;
use App\Telegram\Controllers\MainController;
use App\Telegram\Controllers\RegisterController;
use App\Telegram\Keyboards\Collection\HomeKeyboard;

use App\Telegram\Updates\UpdateAbstract;

class StartHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->answer(
            text: 'Для регистрации в боте tattoo вы должны ввести свой год рождения'
        );

        $update->getUser()->update([
            'controller'=>RegisterController::class,
            'action'=>'birthYear',
        ]);

        return true;
    }
}
