<?php


namespace App\Telegram\Handlers\Report;


use App\Telegram\Controllers\ReportController;
use App\Telegram\Handlers\HandlerClassAbstract;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;

class ReportMainHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->answer(
            text: "Введите начальную дату в формате <b>ДД.ММ.ГГГГ</b>

                   Либо введите начальную и конечную дату в формате <b>ДД.ММ.ГГГГ-ДД.ММ.ГГГГ</b>",
            keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
            parseMode: MessageUpdate::MODE_HTML,
        );

        $update->getUser()->update(['controller'=>ReportController::class, 'action'=>'startDate']);
        return true;
    }
}