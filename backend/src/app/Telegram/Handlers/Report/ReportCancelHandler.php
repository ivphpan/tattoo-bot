<?php


namespace App\Telegram\Handlers\Report;


use App\Telegram\Controllers\MainController;
use App\Telegram\Handlers\HandlerClassAbstract;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Updates\UpdateAbstract;

class ReportCancelHandler extends HandlerClassAbstract
{
    public function call(UpdateAbstract $update): bool
    {
        $update->getUserData()->destroy();

        $update->getUser()->update([
            'controller'=>MainController::class,
            'action'=>'index',
        ]);

        $update->answer('Вы отменили операцию с отчётами.', new HomeKeyboard($update->getUser()));

        return true;
    }
}