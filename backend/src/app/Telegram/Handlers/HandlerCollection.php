<?php


namespace App\Telegram\Handlers;

use App\Telegram\Updates\UpdateAbstract;
use \Closure;

/**
 * Class HandlerCollection
 * @package App\Telegram\Handlers
 *
 * @property HandlerItem[] $handlerList
 */
class HandlerCollection
{
    private array $handlerList = [];
    private string $updateType;

    public function __construct(string $updateType)
    {
        $this->updateType = $updateType;
    }

    public function addClass(string $condition, HandlerClassAbstract $class): void
    {
        $this->handlerList[] = new HandlerItem(
            $condition,
            class: $class
        );
    }

    public function addCallback(string $condition, Closure $callback): void
    {
        $this->handlerList[] = new HandlerItem(
            $condition,
            callback: $callback
        );
    }

    public function call(UpdateAbstract $update):bool
    {
        if (!$this->checkUpdateType($update)){
            return false;
        }

        foreach($this->handlerList as $handler)
        {
            if ($handler->check($update->getText()))
            {
                return $handler->call($update);
            }
        }

        return false;
    }

    public function checkUpdateType(UpdateAbstract $update):bool
    {
        return $this->updateType == $update->getType();
    }
}