<?php


namespace App\Telegram\Controllers;


use App\Components\Number;
use App\Models\Income;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Updates\UpdateAbstract;
use Illuminate\Support\Facades\Validator;

class IncomeController extends MainController
{
    public function sum()
    {
        $this->messageHandler->addCallback('#Отмена#', function (UpdateAbstract $update) {
            $update->getUser()->update([
                'controller' => MainController::class,
                'action' => 'index',
            ]);

            $update->answer('Вы отменили операцию с доходами.', new HomeKeyboard($update->getUser()));

            return true;
        });

        $this->messageHandler->addCallback('#.*#', function (UpdateAbstract $update) {
            $sum = $update->getText();
            $sum = Number::make($sum);

            $validator = Validator::make(['text' => $sum], [
                'text' => 'numeric|min:1|max:9999999999999999',
            ]);

            if (!$validator->fails()) {
                $user = $update->getUser();
                $incomeSum = Number::format($sum);
                $user->incomes()->save(
                    new Income([
                        'sum' => $sum,
                    ])
                );

                $user->update([
                    'controller' => MainController::class,
                    'action' => 'index',
                    'balance' => $user->balance + $sum,
                ]);

                $update->answer(
                    text: "<b>Ваш доход успешно учтён!</b>

                          Сумма:
                          <b>{$incomeSum} {$update->getUser()->currency}</b>",
                    keyboard: new HomeKeyboard($user),
                    parseMode: UpdateAbstract::MODE_HTML,
                );

                $update->getUserData()->destroy();

                return true;
            }

            return false;
        });
    }
}