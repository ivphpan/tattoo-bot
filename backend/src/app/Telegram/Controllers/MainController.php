<?php


namespace App\Telegram\Controllers;


use App\Telegram\Handlers\Consumption\ConsumptionMainHandler;
use App\Telegram\Handlers\Income\IncomeMainHandler;
use App\Telegram\Handlers\Report\ReportMainHandler;
use App\Telegram\Handlers\SettingsHandler;
use App\Telegram\Handlers\StartHandler;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Keyboards\InlineKeyboard;
use App\Telegram\Updates\UpdateAbstract;

class MainController extends ControllerAbstract
{
    public function index()
    {
        $this->messageHandler->addCallback('#.*#', function(UpdateAbstract $update){
            $update->answer('Здесь скоро будет что-то интересное 💋💋💋');
            return true;
        });
    }
}
