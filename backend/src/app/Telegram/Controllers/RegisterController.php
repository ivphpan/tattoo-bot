<?php

namespace App\Telegram\Controllers;


use App\Components\Number;
use App\Telegram\Keyboards\Collection\HomeKeyboard;

use App\Telegram\Updates\UpdateAbstract;
use Illuminate\Support\Facades\Validator;

class RegisterController extends ControllerAbstract
{

    public function birthYear()
    {
        $this->messageHandler->addCallback('#^\d{4}$#', function (UpdateAbstract $update) {

            $currentYear = date('Y');
            $minYear = $currentYear - 100;
            $maxYear = $currentYear - 18;

            $validator = Validator::make([
                'year' => $update->getText(),
            ], [
                'year' => "required|integer|min:{$minYear}|max:{$maxYear}",
            ]);

            if ($validator->fails()) {
                $update->answer('Ваш возраст не соответствует нормам');
                return true;
            }

            $update->answer(
                text: 'Отлично, теперь введите своё реальное имя',
            );

            $update->getUser()->update([
                'action'=>'realname',
            ]);

            return true;
        });
    }

    public function realname()
    {
        $this->messageHandler->addCallback('#^[а-я]{4,16}$#iu', function(UpdateAbstract $update){
            $realname = $update->getText();

            $update->answer(
                text: $realname . ' рад нашему знакомству, меня зовут TattooBot. Я помогу тебе выбрать татуировку и записаться к тату мастеру 😏'
            );

            $update->getUser()->update([
                'realname'=>$update->getText(),
                'controller'=>MainController::class,
                'action'=>'index',
            ]);

            return true;
        });
    }
}
