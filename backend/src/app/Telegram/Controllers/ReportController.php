<?php


namespace App\Telegram\Controllers;


use App\Components\Number;
use App\Models\TelegramUserConsumption;
use App\Repository\TelegramUserConsumptionRepository;
use App\Telegram\Handlers\HandlerCollection;
use App\Telegram\Handlers\Report\ReportCancelHandler;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;
use Illuminate\Support\Carbon;

class ReportController extends MainController
{
    private TelegramUserConsumptionRepository $telegramUserConsumptionRepository;

    public function __construct(HandlerCollection $messageHandler, HandlerCollection $callbackHandler)
    {
        parent::__construct($messageHandler, $callbackHandler);
        $this->telegramUserConsumptionRepository = new TelegramUserConsumptionRepository();
    }

    public function beforeAction()
    {
        parent::beforeAction();
        $this->messageHandler->addClass('#Отмена#', new ReportCancelHandler());
    }

    public function startDate()
    {
        $this->messageHandler->addCallback('#^(\d{2}\.\d{2}\.\d{4})(\s|\-)(\d{2}\.\d{2}\.\d{4})$#',
            function (UpdateAbstract $update) {
                $date = $update->getText();
                if (preg_match('#(\d{2}\.\d{2}\.\d{4})(\s|\-)(\d{2}\.\d{2}\.\d{4})#', $date, $matches)) {
                    $startDate = $matches[1];
                    $endDate = $matches[3];
                    $startDate = Carbon::parse($startDate);
                    $endDate = Carbon::parse($endDate);

                    $reportItems = $this->telegramUserConsumptionRepository->getGroupSum($update->getUser()->id,
                        $startDate,
                        $endDate);

                    if ($reportItems->isEmpty()) {
                        $update->answer(
                            text: "За период <b>{$startDate->isoFormat('DD.MM.Y')}-{$endDate->isoFormat('DD.MM.Y')}</b> ничего не найдено.

                                   Введите начальную дату в формате <b>ДД.ММ.ГГГГ</b>

                                   Либо введите начальную и конечную дату в формате <b>ДД.ММ.ГГГГ-ДД.ММ.ГГГГ</b>",
                            keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
                            parseMode: UpdateAbstract::MODE_HTML,
                        );

                        return true;
                    }

                    $update->getUserData()->set('reportStartDate', $startDate);
                    $update->getUserData()->set('reportEndDate', $endDate);

                    $update->answer(
                        text: 'Выберите тип отчётности',
                        keyboard: ReplyKeyboard::make()
                        ->button('🧾 Суммарный')
                        ->newLine()
                        ->button('🔄 Отмена')
                    );

                    $update->getUser()->update(['action' => 'document']);
                }

                return true;
            });

        $this->messageHandler->addCallback('#^\d{2}\.\d{2}\.\d{4}$#', function (UpdateAbstract $update) {
            $startDate = $update->getText();

            $update->getUserData()->set('reportStartDate', $startDate);

            $update->answer(
                text: "Введите конечную дату в формате <b>ДД.ММ.ГГГГ</b>",
                keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
                parseMode: UpdateAbstract::MODE_HTML
            );

            $update->getUser()->update([
                'action' => 'endDate',
            ]);

            return true;
        });
    }

    public function endDate()
    {
        $this->messageHandler->addCallback('#^\d{2}\.\d{2}\.\d{4}$#', function (UpdateAbstract $update) {
            $endDate = $update->getText();
            $update->getUserData()->set('reportEndDate', $endDate);

            $startDate = Carbon::parse($update->getUserData()->get('reportStartDate'));
            $endDate = Carbon::parse($endDate);

            $reportItems = $this->telegramUserConsumptionRepository->getGroupSum($update->getUser()->id, $startDate,
                $endDate);

            if ($reportItems->isEmpty()) {
                $update->answer(
                    text: "За период <b>{$startDate->isoFormat('DD.MM.Y')}-{$endDate->isoFormat('DD.MM.Y')}</b> ничего не найдено.

                    Введите начальную дату в формате <b>ДД.ММ.ГГГГ</b>
                    
                    Либо введите начальную и конечную дату в формате <b>ДД.ММ.ГГГГ-ДД.ММ.ГГГГ</b>",
                    keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
                    parseMode: UpdateAbstract::MODE_HTML,
                );

                $update->getUser()->update(['action' => 'startDate']);
                return true;
            }

            $update->answer(
                text: 'Выберите тип отчётности',
                keyboard: ReplyKeyboard::make()
                ->button('🧾 Суммарный')
                ->newLine()
                ->button('🔄 Отмена')
            );

            $update->getUser()->update([
                'action' => 'document',
            ]);

            return true;
        });
    }

    public function document()
    {
        $this->messageHandler->addCallback('#Суммарный#', function (UpdateAbstract $update) {
            $userData = $update->getUserData();
            $startDate = Carbon::parse($userData->get('reportStartDate'));
            $endDate = Carbon::parse($userData->get('reportEndDate'));

            $reportItems = $this->telegramUserConsumptionRepository->getGroupSum($update->getUser()->id, $startDate,
                $endDate);

            $text = "<b>Суммарный отчёт</b>
                     <b>{$startDate->isoFormat('DD.MM.Y')}-{$endDate->isoFormat('DD.MM.Y')}</b>\n\n";
            $total = 0;
            foreach ($reportItems as $reportItem) {
                /** @var $reportItem TelegramUserConsumption */
                $groupSum = Number::format($reportItem->groupSum);
                $text .= $reportItem->consumptionGroup->name;
                $text .= " <b>-{$groupSum} {$update->getUser()->currency}</b> ({$reportItem->groupCount})\n";
                $total += $reportItem->groupSum;
            }
            $total = Number::format($total);
            $text .= "\n Общий расход: <b>-{$total} {$update->getUser()->currency}</b>";

            $update->answer(
                text: $text,
                keyboard: new HomeKeyboard($update->getUser()),
                parseMode: UpdateAbstract::MODE_HTML
            );

            $update->getUser()->update([
                'controller' => MainController::class,
                'action' => 'index',
            ]);

            $userData->destroy();

            return true;
        });
    }
}