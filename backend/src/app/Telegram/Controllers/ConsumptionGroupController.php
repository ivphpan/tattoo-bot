<?php


namespace App\Telegram\Controllers;

use App\Models\Consumption;
use App\Models\ConsumptionGroup;
use App\Components\Text;
use App\Repository\ConsumptionGroupRepository;
use App\Telegram\Handlers\Consumption\ConsumptionCancelHandler;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;
use Illuminate\Support\Facades\Validator;

class ConsumptionGroupController extends MainController
{
    public function beforeAction()
    {
        parent::beforeAction();

        $this->messageHandler->addClass('#Отмена#', new ConsumptionCancelHandler());

        $this->messageHandler->addCallback('#Воспользоваться поиском#', function(UpdateAbstract $update){
             $update->answer(
                 text: 'Введите текст для поиска группы расходов',
                 keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
             );

             $update->getUser()->update([
                 'action'=>'search',
             ]);

             return true;
        });

        $this->messageHandler->addCallback('#Добавить группу расходов#', function (UpdateAbstract $update) {
            $update->answer(
                text: "Введите наименование новой группы",
                keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
                parseMode: UpdateAbstract::MODE_HTML
            );

            $update->getUser()->update([
                'action' => 'create',
            ]);

            return true;
        });
    }

    public function search()
    {
        $this->messageHandler->addCallback('#^[^\d]+.*$#iu', function (UpdateAbstract $update) {
            $groupName = $update->getText();

            $validator = Validator::make(['text' => $groupName], [
                'text' => 'required|string|min:2|max:32',
            ]);

            if (!$validator->fails()) {
                $consumptionGroupRepository = new ConsumptionGroupRepository();

                $consumptionGroups = $consumptionGroupRepository->getAllByName($update->getUser()->id, $groupName);

                if ($consumptionGroups->isNotEmpty()) {
                    $keyboard = ReplyKeyboard::make();
                    foreach ($consumptionGroups as $i => $consumptionGroup) {
                        $keyboard->button(
                            text: $consumptionGroup->name
                        );

                        if (($i + 1) % 2 == 0) {
                            $keyboard->newLine();
                        }
                    }
                    $keyboard->newLine()
                        ->button('➕ Добавить группу расходов')
                        ->newLine()
                        ->button('🔎 Воспользоваться поиском')
                        ->newLine()
                        ->button('🔄 Отмена');

                    $update->answer(
                        text: "Выберите группу расходов",
                        keyboard: $keyboard,
                        parseMode: UpdateAbstract::MODE_HTML
                    );

                    $update->getUser()->update(['action'=>'select']);
                } else {
                    $update->answer(
                        text: "Группы расходов с наименованием <b>{$groupName}</b> не найдены",
                        keyboard: ReplyKeyboard::make()
                        ->button('➕ Добавить группу расходов')
                        ->newLine()
                        ->button('🔎 Воспользоваться поиском')
                        ->newLine()
                        ->button('🔄 Отмена'),
                        parseMode: UpdateAbstract::MODE_HTML
                    );
                }

                return true;
            }
        });
    }

    public function select()
    {
        $this->messageHandler->addCallback('#.*#', function (UpdateAbstract $update) {
            $userData = $update->getUserData();

            $consumptionGroupName = $update->getText();
            $consumptionName = $userData->get('consumptionName');

            $validator = Validator::make([
                'consumptionGroupName'=>$consumptionGroupName,
            ], [
                'consumptionGroupName'=>'string|min:2|max:32',
            ]);

            if (!$validator->fails()) {
                $consumptionGroupRepository = new ConsumptionGroupRepository();
                $consumptionGroup = $consumptionGroupRepository->getByName($update->getUser()->id,
                    $consumptionGroupName);

                if ($consumptionGroup) {
                    $consumption = Consumption::create([
                        'name' => $consumptionName,
                        'group_id'=>$consumptionGroup->id,
                        'telegram_user_id'=>$update->getUser()->id,
                    ]);

                    $userData->set('consumptionId', $consumption->id);
                    $userData->set('consumptionGroupId', $consumptionGroup->id);

                    $update->getUser()->update([
                        'controller' => ConsumptionController::class,
                        'action' => 'sum',
                    ]);

                    $update->answer(
                        text: "Введите сумму расхода",
                        keyboard: ReplyKeyboard::make()->button('🔄 Отмена')
                    );

                    return true;
                }
            }
        });
    }

    public function create()
    {
        $this->messageHandler->addCallback('#^[^\d]+.*$#iu', function (UpdateAbstract $update) {
            $consumptionGroupName = $update->getText();
            $consumptionGroupName = Text::ucfirst($consumptionGroupName);

            $validator = Validator::make(['name' => $consumptionGroupName], [
                'name' => 'required|string|min:3|max:16|unique:consumption_groups',
            ]);

            if (!$validator->fails()) {
                $userData = $update->getUserData();
                $consumptionName = $userData->get('consumptionName');

                $consumptionGroup = ConsumptionGroup::create([
                    'name' => $consumptionGroupName,
                    'telegram_user_id'=>$update->getUser()->id,
                ]);

                $consumption = Consumption::create([
                    'telegram_user_id'=>$update->getUser()->id,
                    'name' => $consumptionName,
                    'group_id' => $consumptionGroup->id
                ]);

                $userData->set('consumptionId', $consumption->id);
                $userData->set('consumptionGroupId', $consumptionGroup->id);

                $update->getUser()->update([
                    'controller' => ConsumptionController::class,
                    'action' => 'sum',
                ]);

                $update->answer(
                    text: "Введите сумму расхода",
                    keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
                    parseMode: UpdateAbstract::MODE_HTML
                );

                return true;
            }
        });
    }
}