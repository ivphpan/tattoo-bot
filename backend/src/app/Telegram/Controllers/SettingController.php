<?php


namespace App\Telegram\Controllers;


use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;
use Illuminate\Support\Facades\Validator;

class SettingController extends MainController
{
    public function beforeAction()
    {
        parent::beforeAction();

        $this->messageHandler->addCallback('#Отмена#', function (UpdateAbstract $update) {
            $update->answer(
                text: 'Вы отменили операцию с настройками',
                keyboard: new HomeKeyboard($update->getUser())
            );
            $update->getUser()->update(['controller' => MainController::class, 'action' => 'index']);

            return true;
        });
    }

    public function index()
    {
        $this->messageHandler->addCallback('#Настройка валюты#', function (UpdateAbstract $update) {
            $update->answer(
                text: "Введите название отображаемой валюты <i>Не более 7 символов</i>

                       Например: <i>(руб, грн, сум)</i>",
                keyboard: ReplyKeyboard::make()
                ->button('🔄 Отмена'),
                parseMode: UpdateAbstract::MODE_HTML
            );

            $update->getUser()->update(['action' => 'currency']);

            return true;
        });
    }

    public function currency()
    {
        $this->messageHandler->addCallback('#^[a-zа-я\.]+$#iu', function (UpdateAbstract $update) {
            $currency = $update->getText();

            $validator = Validator::make(['currency' => $currency], [
                'currency' => 'string|max:7',
            ]);

            if (!$validator->fails()) {
                $update->getUser()->update([
                    'currency' => $currency,
                    'controller' => MainController::class,
                    'action' => 'index',
                ]);

                $update->answer(
                    text: "Настройки валюты успешно изменены!",
                    keyboard: new HomeKeyboard($update->getUser()),
                    parseMode: UpdateAbstract::MODE_HTML
                );

                return true;
            }
        });
    }
}