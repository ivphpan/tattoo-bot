<?php


namespace App\Telegram\Controllers;


use App\Components\Number;
use App\Models\Consumption;

use App\Models\TelegramUserConsumption;
use App\Repository\ConsumptionRepository;
use App\Repository\TelegramUserConsumptionRepository;
use App\Telegram\Handlers\Consumption\ConsumptionCancelHandler;
use App\Telegram\Keyboards\Collection\HomeKeyboard;
use App\Telegram\Keyboards\ReplyKeyboard;

use App\Telegram\Updates\UpdateAbstract;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Components\Text;

class ConsumptionController extends MainController
{
    public function beforeAction()
    {
        parent::beforeAction();
        $this->messageHandler->addClass('#Отмена#', new ConsumptionCancelHandler());

        $this->messageHandler->addCallback('#(Воспользоваться поиском)#', function (UpdateAbstract $update) {
            $update->answer(
                text: 'Введите текст для поиска статьи расходов',
                keyboard: ReplyKeyboard::make()->button('🔄 Отмена')
            );

            $update->getUser()->update(['action' => 'search']);

            return true;
        });

        $this->messageHandler->addCallback('#Добавить статью расходов#', function (UpdateAbstract $update) {
            $update->answer(
                text: 'Введите название новой статьи расходов',
                keyboard: ReplyKeyboard::make()->button('🔄 Отмена'),
            );

            $update->getUser()->update(['action' => 'create']);

            return true;
        });
    }

    public function search()
    {
        $this->messageHandler->addCallback('#.*#', function (UpdateAbstract $update) {
            $validator = Validator::make(['text' => $update->getText()], [
                'text' => 'required|string|min:2|max:32'
            ]);

            if (!$validator->fails()) {
                $validated = $validator->validated();
                $name = $validated['text'];

                $consumptionRepository = new ConsumptionRepository();
                $consumptionItems = $consumptionRepository->getAllByName($update->getUser()->id, $name);

                if ($consumptionItems->isNotEmpty()) {
                    $keyboard = ReplyKeyboard::make();
                    foreach ($consumptionItems as $i => $consumptionItem) {
                        /** @var $consumptionItem Consumption */
                        $keyboard->button(
                            text: $consumptionItem->name
                        );

                        if (($i + 1) % 2 == 0) {
                            $keyboard->newLine();
                        }
                    }
                    $keyboard
                        ->newLine()
                        ->button('🔎 Воспользоваться поиском')
                        ->newLine()
                        ->button('➕ Добавить статью расхода')
                        ->newLine()
                        ->button('🔄 Отмена');

                    $update->answer(
                        text: 'Введите текст для поиска статьи расходов',
                        keyboard: $keyboard,
                        parseMode: UpdateAbstract::MODE_HTML
                    );

                    $update->getUser()->update([
                        'action' => 'select',
                    ]);
                } else {
                    $update->answer(
                        text: "Статьи расходов с наименованием <b>{$name}</b> не найдены",
                        keyboard: ReplyKeyboard::make()
                        ->button('➕ Добавить статью расходов')
                        ->newLine()
                        ->button('🔎 Воспользоваться поиском')
                        ->newLine()
                        ->button('🔄 Отмена'),
                        parseMode: UpdateAbstract::MODE_HTML
                    );
                }

                return true;
            }
        });
    }

    public function select()
    {
        $this->messageHandler->addCallback('#.*#', function (UpdateAbstract $update) {
            $consumptionName = $update->getText();

            $validator = Validator::make([
                'consumptionName' => $consumptionName,
            ], [
                'consumptionName' => 'string|min:2|max:32'
            ]);

            if (!$validator->fails()) {
                $consumptionRepository = new ConsumptionRepository();
                $consumption = $consumptionRepository->getByName($update->getUser()->id, $consumptionName);

                if ($consumption) {

                    $update->getUserData()->set('consumptionId', $consumption->id);
                    $update->getUserData()->set('consumptionGroupId', $consumption->group_id);

                    $update->getUser()->update([
                        'action' => 'sum',
                    ]);

                    $update->answer(
                        text: "Введите сумму расхода",
                        keyboard: ReplyKeyboard::make()
                        ->button('🔄 Отмена'),
                        parseMode: UpdateAbstract::MODE_HTML,
                    );

                    return true;
                }
            }
        });
    }

    public function create()
    {
        $this->messageHandler->addCallback('#^[^\d]+.*$#iu', function (UpdateAbstract $update) {
            $validator = Validator::make(['name' => $update->getText()], [
                'name' => 'required|string|min:3|max:32|unique:consumptions',
            ]);

            if (!$validator->fails()) {
                $consumptionName = $update->getText();
                $consumptionName = Text::ucfirst($consumptionName);
                $telegramUserConsumptionRepository = new TelegramUserConsumptionRepository();

                $update->getUserData()->set('consumptionName', $consumptionName);

                $startDate = Carbon::now()->subMonth();
                $endDate = Carbon::now()->addDay();

                $consumptionItems = $telegramUserConsumptionRepository->getLastGroupByParams($update->getUser()->id,
                    $startDate, $endDate);

                if ($consumptionItems->isNotEmpty()) {
                    $keyboard = ReplyKeyboard::make();
                    foreach ($consumptionItems as $i => $consumptionItem) {
                        /** @var $consumptionItem TelegramUserConsumption */
                        $keyboard->button(
                            text: $consumptionItem->consumptionGroup->name
                        );

                        if (($i + 1) % 2 == 0) {
                            $keyboard->newLine();
                        }
                    }

                    $keyboard
                        ->newLine()
                        ->button('➕ Добавить группу расходов')
                        ->newLine()
                        ->button('🔎 Воспользоваться поиском')
                        ->newLine()
                        ->button('🔄 Отмена');

                    $update->answer(
                        text: "Выберите группу расходов",
                        keyboard: $keyboard,
                        parseMode: UpdateAbstract::MODE_HTML
                    );

                    $update->getUser()->update([
                        'controller' => ConsumptionGroupController::class,
                        'action' => 'select',
                    ]);
                } else {
                    $update->answer(
                        text: "Введите наименование группы расходов для поиска",
                        keyboard: ReplyKeyboard::make()
                            ->button('➕ Добавить группу расходов')
                            ->newLine()
                            ->button('🔄 Отмена'),
                        parseMode: UpdateAbstract::MODE_HTML
                    );

                    $update->getUser()->update([
                        'controller' => ConsumptionGroupController::class,
                        'action' => 'search',
                    ]);
                }

                return true;
            }
        });
    }

    public function sum()
    {
        $this->messageHandler->addCallback('#.*#', function (UpdateAbstract $update) {
            $sum = $update->getText();
            $sum = Number::make($sum);

            $validator = Validator::make(['sum' => $sum], [
                'sum' => 'numeric|min:1|max:9999999999999999',
            ]);

            if (!$validator->fails()) {
                $user = $update->getUser();
                $userData = $update->getUserData();
                $consumptionId = $userData->get('consumptionId');
                $consumptionGroupId = $userData->get('consumptionGroupId');
                $consumptionSum = Number::format($sum);

                $telegramUserConsumption = TelegramUserConsumption::create([
                    'telegram_user_id' => $user->id,
                    'consumption_id' => $consumptionId,
                    'consumption_group_id' => $consumptionGroupId,
                    'sum' => $sum,
                ]);

                $user->update([
                    'controller' => MainController::class,
                    'action' => 'index',
                    'balance' => $user->balance - $sum,
                ]);
                $text = "<b>Ваш расход успешно учтён!</b>

                           Наименование:
                           <b>{$telegramUserConsumption->consumption->name}</b>
                           
                           Сумма:
                           <b>{$consumptionSum} {$update->getUser()->currency}</b>";

                $update->answer(
                    text: $text,
                    keyboard: new HomeKeyboard($user),
                    parseMode: UpdateAbstract::MODE_HTML
                );

                $update->getUserData()->destroy();

                return true;
            }
        });
    }
}