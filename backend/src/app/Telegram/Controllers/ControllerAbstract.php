<?php


namespace App\Telegram\Controllers;

use App\Telegram\Handlers\HandlerCollection;

abstract class ControllerAbstract
{
    protected HandlerCollection $messageHandler;
    protected HandlerCollection $callbackHandler;

    public function __construct(HandlerCollection $messageHandler, HandlerCollection $callbackHandler)
    {
        $this->messageHandler = $messageHandler;
        $this->callbackHandler = $callbackHandler;
    }

    public function beforeAction()
    {

    }
}