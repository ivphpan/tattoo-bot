<?php


namespace App\Telegram;


use App\Telegram\Components\Response;

class Api
{
    private $token;
    private $method;
    private $params = [];
    private $timeout = 30;
    private $postParams = [];
    private $enviroment = 'dev';

    public function __construct()
    {
        $this->token = env('TELEGRAM_TOKEN');
        $this->enviroment = env('APP_ENV');
    }

    public function setMethod($method): self
    {
        $this->method = $method;
        return $this;
    }

    public function setParams($params): self
    {
        $this->params = $params;
        return $this;
    }

    public function setPostParams($params): self
    {
        $this->postParams = $params;
        return $this;
    }

    public function setTimeout($sec)
    {
        $this->timeout = $sec;
        return $this;
    }

    public function call(): Response
    {
        $apiCallUrl = $this->getApiCallUrl();
//        echo $apiCallUrl."\r\n";
        $curl = \curl_init($apiCallUrl);

        \curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => $this->timeout
        ]);

        if (!empty($this->postParams)) {
            curl_setopt_array($curl, [
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $this->postParams,
            ]);
        }

        $result = \curl_exec($curl);
        $info = curl_getinfo($curl);
//        dump('curlGetInfo', $info);
        \curl_close($curl);
        $result = json_decode($result);
        return new Response($result);
    }

    private function getApiCallUrl()
    {
        if ($this->enviroment == 'testing') {
            $url = route('telegram/server', ['method' => $this->method, 'params' => http_build_query($this->params)]);
        } else {
            $url = 'https://api.telegram.org/bot'.$this->token.'/'.$this->method;
            if (!empty($this->params)) {
                $url .= '?'.http_build_query($this->params);
            }
        }
        return $url;
    }
}
