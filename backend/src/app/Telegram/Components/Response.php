<?php


namespace App\Telegram\Components;

use \stdClass;

class Response
{
    private stdClass $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getMessageId():int
    {
        if (!$this->isOk()) {
            return 0;
        }
        return $this->data->result->message_id;
    }

    public function getResult()
    {
        return $this->data->result;
    }

    public function isOk():bool
    {
        return property_exists($this->data, 'ok') && $this->data->ok===true;
    }
}