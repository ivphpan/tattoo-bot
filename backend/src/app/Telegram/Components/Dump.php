<?php


namespace App\Telegram\Components;


use Illuminate\Support\Facades\Storage;

class Dump
{
    private $path;
    private $content;

    public function __construct()
    {
        $this->path = storage_path('telegram_dumps');
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function save()
    {
        Storage::set($this->path, $this->content);
    }
}