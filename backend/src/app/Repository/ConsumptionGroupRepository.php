<?php


namespace App\Repository;


use App\Models\ConsumptionGroup;

class ConsumptionGroupRepository
{
    public function getByName($telegramUserId, $name)
    {
        return ConsumptionGroup::where('name', $name)
            ->where('telegram_user_id', $telegramUserId)
            ->first();
    }

    public function getAllByName($telegramUserId, $name, $limit = 6)
    {
        return ConsumptionGroup::where('name', 'like', '%'.$name.'%')
            ->where('telegram_user_id', $telegramUserId)
            ->limit($limit)
            ->get();
    }
}