<?php

namespace App\Repository;


use App\Models\TelegramUserConsumption;

class TelegramUserConsumptionRepository
{
    public function getLastConsumptionByParams($userId, $startDate, $endDate, $limit=10)
    {
        return TelegramUserConsumption::where('telegram_user_id', $userId)
            ->with(['consumption'])
            ->select(['consumption_id'])
            ->whereDate('created_at', '>=', $startDate->toDateString())
            ->whereDate('created_at', '<=', $endDate->toDateString())
            ->groupBy('consumption_id')
            ->orderByRaw('max(created_at) DESC')
            ->limit($limit)
            ->get();
    }

    public function getLastGroupByParams($telegramUserId, $startDate, $endDate, $limit=10)
    {
        return TelegramUserConsumption::where('telegram_user_id', $telegramUserId)
            ->with(['consumptionGroup'])
            ->select(['consumption_group_id'])
            ->whereDate('created_at', '>=', $startDate->toDateString())
            ->whereDate('created_at', '<=', $endDate->toDateString())
            ->groupBy('consumption_group_id')
            ->orderByRaw('max(created_at) DESC')
            ->limit($limit)
            ->get();
    }

    public function getAllMonthByDate($telegramUserId, $startDate, $endDate)
    {
        return TelegramUserConsumption::where('telegram_user_id', $telegramUserId)
            ->selectRaw('left(created_at, 7) as shortDate')
            ->whereDate('created_at', '>=', $startDate)
            ->whereDate('created_at', '<=', $endDate)
            ->groupByRaw('shortDate')
            ->orderBy('created_at')
            ->get();
    }

    public function getGroupSum($telegramUserId, $startDate, $endDate)
    {
        return TelegramUserConsumption::where('telegram_user_id', $telegramUserId)
            ->with(['consumptionGroup'])
            ->selectRaw('consumption_group_id, count(*) AS groupCount, sum(`sum`) as groupSum')
            ->whereDate('created_at', '>=', $startDate)
            ->whereDate('created_at', '<=', $endDate)
            ->groupBy('consumption_group_id')
            ->orderByDesc('groupSum')
            ->get();
    }
}