<?php


namespace App\Repository;


use App\Models\Consumption;

class ConsumptionRepository
{
    public function getByName($telegramUserId, $name)
    {
        return Consumption::where('name', $name)
            ->where('telegram_user_id', $telegramUserId)
            ->first();
    }

    public function getAllByName($telegramUserId, $name, $limit = 10)
    {
        return Consumption::where('name', 'like', '%'.$name.'%')
            ->where('telegram_user_id', $telegramUserId)
            ->limit(10)
            ->get();
    }
}