<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TelegramUserConsumptions
 *
 * @property int $id
 * @property int $telegram_user_id Id Телеграм пользователя
 * @property int $consumption_id ID Расхода
 * @property int $consumption_group_id ID Группы расхода
 * @property float $sum Сумма расхода
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Consumption $consumption
 * @property-read \App\Models\ConsumptionGroup $consumptionGroup
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption query()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereConsumptionGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereConsumptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereTelegramUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserConsumption whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Database\Factories\TelegramUserConsumptionFactory factory(...$parameters)
 */
class TelegramUserConsumption extends Model
{
    use HasFactory;

    protected $fillable = [
        'telegram_user_id',
        'consumption_id',
        'consumption_group_id',
        'sum',
    ];

    public function consumption()
    {
        return $this->belongsTo(Consumption::class, 'consumption_id', 'id');
    }

    public function consumptionGroup()
    {
        return $this->belongsTo(ConsumptionGroup::class, 'consumption_group_id', 'id');
    }
}
