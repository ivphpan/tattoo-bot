<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ConsumptionGroup
 *
 * @package App\Models
 * @property int $id
 * @property string $name Название
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ConsumptionGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ConsumptionGroup extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'telegram_user_id'];
}
