<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Consumption
 *
 * @package App\Models
 * @property int $id
 * @property int $group_id ID Группы расходов
 * @property string $name Название
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\ConsumptionFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption query()
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Consumption whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Consumption extends Model
{
    use HasFactory;

    protected $fillable = ['group_id', 'name', 'telegram_user_id'];
}
