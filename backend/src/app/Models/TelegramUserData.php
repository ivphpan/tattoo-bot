<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TelegramUserData
 *
 * @package App\Models
 * @property int $id
 * @property int $telegram_user_id Пользователь
 * @property string $key Ключ
 * @property string $value Значение
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\TelegramUserDataFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData query()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUserData whereTelegramUserId($value)
 */
class TelegramUserData extends Model
{
    use HasFactory;

    protected $fillable = ['telegram_user_id', 'key', 'value'];
}
