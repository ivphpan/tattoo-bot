<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TelegramUser
 *
 * @package App\Models
 * @property int $id Идентификатор
 * @property string $username Телеграм логин
 * @property string $controller Контроллер
 * @property string $action Экшен
 * @property string|null $language_code Язык сообщений
 * @property string|null $first_name Имя
 * @property string|null $last_name Фамилия
 * @property string|null $phone Телефон
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $balance Баланс
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Consumption[] $consumptions
 * @property-read int|null $consumptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TelegramUserData[] $data
 * @property-read int|null $data_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Income[] $incomes
 * @property-read int|null $incomes_count
 * @method static \Database\Factories\TelegramUserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereController($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereLanguageCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereUsername($value)
 * @property string $currency Валюта
 * @method static \Illuminate\Database\Eloquent\Builder|TelegramUser whereCurrency($value)
 */
class TelegramUser extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $fillable = [
        'id',
        'username',
        'first_name',
        'last_name',
        'controller',
        'action',
        'balance',
        'currency',
        'language_code',
        'realname',
    ];

    public function data()
    {
        return $this->hasMany(TelegramUserData::class, 'telegram_user_id', 'id');
    }

    public function consumptions()
    {
        return $this->hasMany(Consumption::class);
    }

    public function consumptionGroups()
    {
        return $this->hasMany(ConsumptionGroup::class);
    }

    public function incomes()
    {
        return $this->hasMany(Income::class, 'telegram_user_id', 'id');
    }
}
