<?php

namespace Database\Seeders;

use App\Models\ConsumptionGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsumptionGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consumption_groups')->truncate();
        $telegramUserId = 288627620;

        $consumptionGroups = [
            ['name'=>'Еда и хозтовары', 'telegram_user_id'=>$telegramUserId],
            ['name'=>'Жильё', 'telegram_user_id'=>$telegramUserId],
            ['name'=>'Дети', 'telegram_user_id'=>$telegramUserId],
        ];

        foreach($consumptionGroups as $consumptionGroup) {
            ConsumptionGroup::create($consumptionGroup);
        }
    }
}
