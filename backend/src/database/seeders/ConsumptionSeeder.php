<?php

namespace Database\Seeders;

use App\Models\Consumption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsumptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consumptions')->truncate();
        $telegramUserId = 288627620;
        $consumptions = [
            [
                'name' => 'Хлеб',
                'group_id' => 1,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Молоко',
                'group_id' => 1,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Фейри для посуды',
                'group_id' => 1,
                'telegram_user_id' => $telegramUserId,
            ],

            [
                'name' => 'Аренда',
                'group_id' => 2,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Свет',
                'group_id' => 2,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Интернет',
                'group_id' => 2,
                'telegram_user_id' => $telegramUserId,
            ],

            [
                'name' => 'Подгузники',
                'group_id' => 3,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Присыпка',
                'group_id' => 3,
                'telegram_user_id' => $telegramUserId,
            ],
            [
                'name' => 'Смесь',
                'group_id' => 3,
                'telegram_user_id' => $telegramUserId,
            ],
        ];

        foreach ($consumptions as $consumption) {
            Consumption::create($consumption);
        }
    }
}
