<?php

namespace Database\Factories;

use App\Models\TelegramUserData;
use Illuminate\Database\Eloquent\Factories\Factory;

class TelegramUserDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TelegramUserData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
