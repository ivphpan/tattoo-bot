<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TelegramUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * id	bigint(20) unsigned	ID Телеграм
        username	varchar(255) NULL	Телеграм логин
        controller	varchar(255)	Контроллер
        action	varchar(255)	Экшен
        language_code	varchar(255) NULL	Язык сообщений
        first_name	varchar(255) NULL	Имя
        last_name	varchar(255) NULL	Фамилия
        phone	varchar(255) NULL	Телефон
        balance	double(20,2) [0.00]	Баланс
        currency	varchar(7) NULL	Валюта
        created_at	timestamp NULL
        updated_at	timestamp NULL

         */
        Schema::create('telegram_users', function (Blueprint $table){
            $table->bigIncrements('id')->unsigned();
            $table->string('controller')->nullable();
            $table->string('action')->nullable();
            $table->string('language_code')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('username')->nullable();
            $table->string('realname')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_users');
    }
}
