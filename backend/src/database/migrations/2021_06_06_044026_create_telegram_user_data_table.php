<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegram_user_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('telegram_user_id')->unsigned()->comment('Пользователь');
            $table->string('key')->comment('Ключ');
            $table->string('value')->comment('Значение');
            $table->timestamps();

            $table->unique(['telegram_user_id', 'key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegram_user_data');
    }
}
