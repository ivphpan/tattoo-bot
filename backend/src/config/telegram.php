<?php

return [
    'token' => env('TELEGRAM_TOKEN'),
    'webhookUrl' => env('TELEGRAM_WEBHOOK_URL'),
    'keyfile' => env('TELEGRAM_SSL_PEM'),
    'debug' => env('TELEGRAM_DEBUG', false),
];