<?php

namespace Tests\Unit;

use App\Components\Number;
use Tests\TestCase;

class NumberTest extends TestCase
{
    public function test_make_number_return_number()
    {
        $number = 123.12;
        $this->assertEquals($number, Number::make($number));
    }

    public function test_make_minus_number_return_minus_number()
    {
        $number = -123.12;
        $this->assertEquals($number, Number::make($number));
    }

    public function test_make_string_return_zero_number()
    {
        $data = 'fjslkfjasl;kfjsdlk;ajfkl;asd';
        $this->assertEquals(0, Number::make($data));
    }

    public function test_make_number_with_comma_return_number()
    {
        $data = '123,13';
        $this->assertEquals(123.13, Number::make($data));
    }

    public function test_make_number_with_many_dotted_return_number()
    {
        $data = '100.000.000';
        $this->assertEquals(100000000, Number::make($data));
    }

    public function test_make_number_with_many_comma_return_number()
    {
        $data = '100,000,000';
        $this->assertEquals(100000000, Number::make($data));
    }

    public function test_make_number_with_end_of_above_return_number()
    {
        $data = '123.123.123.123';
        $this->assertEquals(123123123.123, Number::make($data));
    }

    /**
     * @dataProvider dataProviderNameWithCharacter
     *
     * @param $expected
     * @param $inputData
     */
    public function test_make_number_with_character_return_number($expected, $inputData)
    {
        $this->assertEquals($expected, Number::make($inputData));
    }

    /**
     * Дата провайдер для теста test_number_with_character_return_number
     * @return array[]
     */
    public function dataProviderNameWithCharacter()
    {
        return [
            [
                'expected' => 123.12,
                'inputData' => '123б12',
            ],
            [
                'expected' => 123.12,
                'inputData' => '123ю12',
            ]
        ];
    }

    /**
     * @dataProvider dataProviderFormatNumber
     *
     * @param $number
     * @param $expected
     */
    public function test_format_number_return_formatted_string($number, $expected)
    {
        $this->assertEquals($expected, Number::format($number));
    }

    /**
     * Дата провайдер для теста test_format_number_return_formatted_string
     * @return array[]
     */
    public function dataProviderFormatNumber()
    {
        return [
            [
                'number' => 123123123,
                'expected' => '123 123 123',
            ],
            [
                'number' => 123123.123,
                'expected' => '123 123.12',
            ],
            [
                'number' => -123123123,
                'expected' => '-123 123 123',
            ],
            [
                'number' => -123123123.123,
                'expected' => '-123 123 123.12',
            ],
            [
                'number' => 123123123.00,
                'expected' => '123 123 123',
            ],
            [
                'number' => -123123123.00,
                'expected' => '-123 123 123',
            ]
        ];
    }
}
