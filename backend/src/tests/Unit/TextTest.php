<?php

namespace Tests\Unit;

use App\Components\Text;
use Tests\TestCase;

class TextTest extends TestCase
{
    public function test_normal_return_string(){
        $text = 'Строка один 10 грамм.
                Строка два 20 грамм.';
        $expected = 'Строка один 10 грамм.
Строка два 20 грамм.';
        $this->assertEquals($expected, Text::make($text));
    }

    public function test_string_start_with_number_return_string()
    {
        $text = '100 рублей';
        $expected = 'рублей';
        $this->assertEquals($expected, Text::make($text));
    }
    
    public function test_string_with_first_tag_return_string()
    {
        $text = '<b>Строка один 10 грамм.</b>
                Строка два 20 грамм.';
        $expected = '<b>Строка один 10 грамм.</b>
Строка два 20 грамм.';
        
        $this->assertEquals($expected, Text::make($text));
    }
}
