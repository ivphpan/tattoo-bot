<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FreeController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\LetsEncryptController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FreeController::class, 'index']);
Route::get('/free', [FreeController::class, 'index']);
Route::get('/telegram/webhook-info', [TelegramController::class, 'getWebhookInfo']);

Route::post('/telegram/webhook-handle', [TelegramController::class, 'handleWebhook']);
Route::post('/telegram/server', [TelegramController::class, 'server'])->name('telegram-server');
